CC=gcc
CXX=g++
LEX=flex
YACC=bison

CXXFLAGS=-g3 -O0

all: hw4.exe

hw4.exe: lex.yy.o hw4.o hw4.tab.o
	$(CXX) -o $@ $(LDFLAGS) $^

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $< -o $@

lex.yy.cpp: hw4.lex hw4.tab.cpp
	$(LEX) -o $@ $<

hw4.tab.cpp: hw4.ypp
	$(YACC) --graph -v -d $<

graph: hw4.tab.cpp
	dot -Tjpg hw4.dot -o hw4.jpg

clean:
	rm -f hw4 hw4.exe *.o hw4.tab.* lex.yy.* *.jpg *.dot *.output

.PHONY:	all clean graph
