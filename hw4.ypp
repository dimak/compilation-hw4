%{

#include <iostream>
#include <map>

#include "hw4.hpp"

using namespace std;

static map<string, expr> variables;

extern int yylex();
static void yyerror(const char *);

static void check_not_etype(etype_t type, const yystype &yys);
static void check_etype(etype_t type, const yystype &yys);

static void arithop(yystype &yyres, const yystype &yya, const yystype &yyop, const yystype &yyb);
static void relop(yystype &yyres, const yystype &yya, const yystype &yyop, const yystype &yyb);
static void logicop(yystype &yyres, const yystype &yya, const yystype &yyop, const yystype &yyb);

static void print_exp(const expr &yys);
static void read_var(const string &name);
static void store_var(const string &name, const expr &yys);
static void retrieve_var(const string &name, yystype &yys);

static void get_length(yystype &yya, const yystype &yys);
static void concat(yystype &yya, const yystype &yys1, const yystype &yys2);

static void init_expr(yystype &yys);
static void init_stmt(yystype &yys);

static void do_stmt(const yystype &yys);

%}

%nonassoc PRINT
%nonassoc READ
%nonassoc LEN
%nonassoc IF
%nonassoc TRUE
%nonassoc FALSE
%nonassoc NUM
%nonassoc STRING
%nonassoc ID
%nonassoc SC
%nonassoc LP
%nonassoc RP
%nonassoc COMMENT
%nonassoc ASSIGN

/* Sorted by operator precedence */
%left LOGIC_OP_OR
%left LOGIC_OP_AND
%left REL_OP_EQ
%left REL_OP_CMP
%left STR_CON
%left ARITH_OP_ADD
%left ARITH_OP_MUL


%%

prog   : stmts
       ;

stmts  : stmts stmt_done
       | stmt_done
       ;

stmt_done : stmt { do_stmt($1); }

stmt   : READ ID SC {
                init_stmt($$);
                $$.s.type = STYPE_READ;
                $$.s.op1 = $2.e;
        }
       | PRINT exp SC {
                check_not_etype(ETYPE_BOOL, $2);

                init_stmt($$);
                $$.s.type = STYPE_PRINT;
                $$.s.op1 = $2.e;
        }
       | ID ASSIGN exp SC {
                init_stmt($$);
                $$.s.type = STYPE_ASSIGN;
                $$.s.op1 = $1.e;
                $$.s.op2 = $3.e;
        }
       | IF LP exp RP stmt {
                check_etype(ETYPE_BOOL, $3);

                init_stmt($$);
                if ($3.e.bval) {
                        $$ = $5;
                } else {
                        $$.s.type = STYPE_NONE;
                }
        }
       ;

exp    : ID { retrieve_var($1.e.sval, $$); }
       | NUM  { init_expr($$); $$.e.type = ETYPE_INT; $$.e.ival = $1.e.ival; }
       | LP exp RP  { $$ = $2; }
       | exp ARITH_OP_MUL exp { arithop($$, $1, $2, $3); }
       | exp ARITH_OP_ADD exp { arithop($$, $1, $2, $3); }
       | STRING { init_expr($$); $$.e.type = ETYPE_STRING; $$.e.sval = $1.e.sval; }
       | LEN LP exp RP { get_length($$, $3); }
       | exp STR_CON exp { concat($$, $1, $3); }
       | TRUE   { init_expr($$); $$.e.type = ETYPE_BOOL; $$.e.bval = true; }
       | FALSE  { init_expr($$); $$.e.type = ETYPE_BOOL; $$.e.bval = false; }
       | exp REL_OP_CMP exp { relop($$, $1, $2, $3); }
       | exp REL_OP_EQ exp { relop($$, $1, $2, $3); }
       | exp LOGIC_OP_OR exp { logicop($$, $1, $2, $3); }
       | exp LOGIC_OP_AND exp { logicop($$, $1, $2, $3); }
       ;

%%

static void check_not_etype(etype_t type, const yystype &yys) {
        if (yys.type != TYPE_EXPR || yys.e.type == type) {
                errSem();
        }
}

static void check_etype(etype_t type, const yystype &yys) {
        if (yys.type != TYPE_EXPR || yys.e.type != type) {
                errSem();
        }
}

static int _arithop(int op, int a, int b) {
        switch (op) {
        case ARITH_ADD:
                return a + b;
        case ARITH_SUB:
                return a - b;
        case ARITH_MUL:
                return a * b;
        case ARITH_DIV:
                return a / b;
        }
}

static void arithop(yystype &res, const yystype &a, const yystype &op, const yystype &b) {
        check_etype(ETYPE_INT, a);
        check_etype(ETYPE_INT, b);

        init_expr(res);
        res.e.type = ETYPE_INT;
        res.e.ival = _arithop(op.e.ival, a.e.ival, b.e.ival);
}


static bool _relop(int op, int a, int b) {
        switch (op) {
        case REL_EQ:
                return a == b;
        case REL_NE:
                return a != b;
        case REL_LE:
                return a <= b;
        case REL_GE:
                return a >= b;
        case REL_LT:
                return a < b;
        case REL_GT:
                return a > b;
        }
}

static void relop(yystype &yyres, const yystype &a, const yystype &op, const yystype &b) {
        check_etype(ETYPE_INT, a);
        check_etype(ETYPE_INT, b);

        init_expr(yyres);
        yyres.e.type = ETYPE_BOOL;
        yyres.e.bval = _relop(op.e.ival, a.e.ival, b.e.ival);
}

static bool _logicop(int op, bool a, bool b) {
        switch (op) {
        case LOGIC_AND:
                return a && b;
        case LOGIC_OR:
                return a || b;
        }
}

static void logicop(yystype &yyres, const yystype &a, const yystype &op, const yystype &b) {
        check_etype(ETYPE_BOOL, a);
        check_etype(ETYPE_BOOL, b);

        init_expr(yyres);
        yyres.e.type = ETYPE_BOOL;
        yyres.e.bval = _logicop(op.e.ival, a.e.bval, b.e.bval);
}

static void print_exp(const expr &e) {
        switch (e.type) {
        case ETYPE_INT:
                cout << e.ival;
                break;
        case ETYPE_STRING:
                cout << e.sval;
                break;
        }
        cout << endl;
}

static void read_var(const string &name) {
	expr e;

        e.type = ETYPE_STRING;
        getline(cin, e.sval);

        if (e.sval.size()) {
                const char *csval = e.sval.c_str();
                char *csval_end;

                int ival = strtol(csval, &csval_end, 10);

                if ((csval_end - csval) == e.sval.size()) {
                        e.type = ETYPE_INT;
                        e.ival = ival;
                }
        }

        variables[name] = e;
}


static void store_var(const string &name, const expr &e) {
        variables[name] = e;
}

static void retrieve_var(const string &name, yystype &yys) {
        if (variables.find(name) == variables.end()) {
                errSem();
        }
        init_expr(yys);
        yys.e = variables[name];
}

static void get_length(yystype &yya, const yystype &yys) {
        check_etype(ETYPE_STRING, yys);
        init_expr(yya);
        yya.e.type = ETYPE_INT;
        yya.e.ival = yys.e.sval.size();
}

static void concat(yystype &yya, const yystype &yys1, const yystype &yys2) {
        check_etype(ETYPE_STRING, yys1);
        check_etype(ETYPE_STRING, yys2);
        init_expr(yya);
        yya.e.type = ETYPE_STRING;
        yya.e.sval = yys1.e.sval + yys2.e.sval;
}

static void yyerror(const char *error) {
        errSyn();
}

static void init_expr(yystype &yys) {
        yys.type = TYPE_EXPR;
}

static void init_stmt(yystype &yys) {
        yys.type = TYPE_STMT;
}

static void do_stmt(const yystype &yys) {
        switch (yys.s.type) {
        case STYPE_READ:
                read_var(yys.s.op1.sval);
                break;

        case STYPE_ASSIGN:
                store_var(yys.s.op1.sval, yys.s.op2);
                break;

        case STYPE_PRINT:
                print_exp(yys.s.op1);
                break;
        }
}
