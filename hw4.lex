%{
#include "hw4.hpp"
#include "hw4.tab.hpp"
%}

%option yylineno
%option noyywrap

int             (0|[1-9][0-9]*)
string          (\"[^"]*\")
id              ([a-zA-Z][a-zA-Z0-9]*)
rel_op_cmp      (<=|>=|<|>)
rel_op_eq       (==|!=)
logic_op_and    (&&)
logic_op_or     (\|\|)
arith_op_mul    ([*/])
arith_op_add    ([+\-])
comment         (\/\/.*)

%%

print           { return PRINT; }
read            { return READ; }
len             { return LEN; }
if              { return IF; }
true            { return TRUE; }
false           { return FALSE; }
{int}           {
        yylval.e.ival = atoi(yytext);
        return NUM;
}
{string}        {
        yylval.e.sval = string(yytext, 1, strlen(yytext) - 2);
        return STRING;
}
{id}            {
        yylval.e.sval = string(yytext);
        return ID;
}
{rel_op_cmp}    {
        if (!strcmp(yytext, "<=")) {
                yylval.e.ival = REL_LE;
        } else if (!strcmp(yytext, ">=")) {
                yylval.e.ival = REL_GE;
        } else if (!strcmp(yytext, "<")) {
                yylval.e.ival = REL_LT;
        } else if (!strcmp(yytext, ">")) {
                yylval.e.ival = REL_GT;
        }
        return REL_OP_CMP;
}
{rel_op_eq}     {
        if (!strcmp(yytext, "==")) {
                yylval.e.ival = REL_EQ;
        } else if (!strcmp(yytext, "!=")) {
                yylval.e.ival = REL_NE;
        }
        return REL_OP_EQ;
}
{logic_op_and}      {
        yylval.e.ival = LOGIC_AND;
        return LOGIC_OP_AND;
}
{logic_op_or}      {
        yylval.e.ival = LOGIC_OR;
        return LOGIC_OP_OR;
}
{arith_op_mul}  {
        switch (yytext[0]) {
        case '*':
                yylval.e.ival = ARITH_MUL;
                break;
        case '/':
                yylval.e.ival = ARITH_DIV;
                break;
        }
        return ARITH_OP_MUL;
}
{arith_op_add}  {
        switch (yytext[0]) {
        case '+':
                yylval.e.ival = ARITH_ADD;
                break;
        case '-':
                yylval.e.ival = ARITH_SUB;
                break;
        }
        return ARITH_OP_ADD;
}
=               { return ASSIGN; }
;               { return SC; }
\(              { return LP; }
\)              { return RP; }
\.              { return STR_CON; }
{comment}       ;

[ \t\n\r]       { }
.               { errLex(); }

%%

