#include <iostream>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

extern FILE* yyin;
int yyparse();

void errLex(){
    cout << "LEXICAL ERROR" << endl;
    exit(0);
}

void errSyn(){
    cout << "SYNTACTIC ERROR" << endl;
    exit(0);
}

void errSem(){
    cout << "SEMANTIC ERROR" << endl;
    exit(0);
}

int main(int argc, const char* argv[]){
    if(argc != 2){
	cout << "Usage: " << argv[0] << " " << "<SCRIPT FILE>" << endl;
	exit(1);
    }
    yyin = fopen(argv[1], "r");
    return yyparse();
}
