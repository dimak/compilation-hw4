#ifndef _COMPILATION_HW4
#define _COMPILATION_HW4

#include <string>
using namespace std;

void errLex();
void errSyn();
void errSem();

typedef enum {
	TYPE_STMT,
	TYPE_EXPR,
} vtype_t;

typedef enum {
        ETYPE_INT,
        ETYPE_STRING,
        ETYPE_BOOL,
} etype_t;

typedef enum {
	STYPE_NONE,
	STYPE_PRINT,
	STYPE_READ,
	STYPE_ASSIGN,
} stype_t;

typedef struct {
	etype_t  type;
	int     ival;
	string  sval;
	bool    bval;
} expr;

typedef struct yystype_s {
	vtype_t type;
	expr e;
	struct {
		stype_t type;
		expr op1;
		expr op2;
	} s;
} yystype;

#define YYSTYPE yystype

typedef enum {
        REL_EQ,
        REL_NE,
        REL_LE,
        REL_GE,
        REL_LT,
        REL_GT,
} rel_t;

typedef enum {
        LOGIC_AND,
        LOGIC_OR,
} logic_t;

typedef enum {
        ARITH_ADD,
        ARITH_SUB,
        ARITH_DIV,
        ARITH_MUL,
} arith_t;

#endif // _COMPILATION_HW4
